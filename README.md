* Per compilare la routine per il calcolo del tune: ```make``` (è necessario ```g++```, ```gfortran``` e ```libgfortran```. Il codice Python dipende da numpy, scipy e pandas.)
* Per calcolare il tune dato il risultato di un'orbita ```./do_tune.sh orbit_file n_turns n_cell```, dove ```orbit_file``` è il file con i risultati dell'orbita, ```n_turns``` il numero di giri simulati della macchina e ```n_cell``` il numero di celle per giro (per ora 50)
* ```do_tune``` restituisce i valori di Qx e Qy e chiama ```gnuplot``` per generare i grafici dell'orbita nello spazio delle fasi normalizzato e dell'azione in funzione del tempo
* i valori delle funzioni $\beta_x$, $\beta_y$, $\alpha_x$, $\alpha_y$ sono calcolati da ```madx``` nella tabella ```twiss_table.tfs```. Il sistema funziona finché non viene cambiato il modo di generare la cella.

