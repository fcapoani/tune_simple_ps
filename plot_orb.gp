#!/usr/bin/gnuplot -persist

filename=ARG1

set terminal pdfcairo

set output 'orbit_'.filename.'.pdf'
set xlabel 'norm. x, y [mm^{1/2}]'
set ylabel 'norm. p_x, p_y [mm^{1/2}]'


p filename every 30 u ($10*1e3):($11*1e3) w p pt 6 lc "blue" t 'x', "" every 30 u ($12*1e3):($13*1e3) w p pt 7 lc "red" t 'y'

set output 'action_'.filename.'.pdf'

set xlabel 's [m]'
set ylabel 'J_x, J_y [µm]'

set key above
set yrange [0:*]
p filename every 30 u 1:(.5*($10**2 + $11 **2)*1e6) w p pt 6 ps .5 lc "blue" t 'J_x', "" every 30 u 1:(.5*($12**2 + $13**2) *1e6) w p pt 7 ps .5 lc "red" t 'J_y'
