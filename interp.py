import numpy as np
from scipy.interpolate import interp1d
import pandas as pd
import sys

## interp.py orbit_file number_of_turns

data = pd.read_csv('twiss_table.tfs', skiprows=52,delim_whitespace=True, header=None)
ldata = pd.read_csv(sys.argv[1], skiprows=15, delim_whitespace=True, header=None)


s_ = data[1]
bx_ = data[3]
by_ = data[4]
ax_ = data[5]
ay_ = data[6]

s=np.array(s_[0])
bx=np.array(bx_[0])
by=np.array(by_[0])
ax=np.array(ax_[0])
ay=np.array(ay_[0])

x = np.array(ldata[1])
px = np.array(ldata[2])
y = np.array(ldata[3])
py = np.array(ldata[4])


nturns=int(sys.argv[2])
s0=0
s_data=np.array([0.])
for i in range(0,100*nturns):
    for j in range(5):
       s0+=0.4
       if s0 > np.amax(s_)*nturns:
            s0 = np.amax(s_)*nturns
       s_data=np.append(s_data, s0)
    for j in range(10):
       s0+=0.44
       if s0 > np.amax(s_)*nturns:
            s0 = np.amax(s_)*nturns
       s_data=np.append(s_data, s0)


for i in range(0,nturns):
    olds=i*640
    for j in range(1,len(s_)):
        news=s_[j] + 640*i
        if news != olds:
            s=np.append(s, s_[j] + 640*i)
            bx=np.append(bx,bx_[j])
            by=np.append(by,by_[j])
            ax=np.append(ax,ax_[j])
            ay=np.append(ay,ay_[j])
        olds=news 


# Perform spline interpolation
interp_bx = interp1d(s, bx, kind='cubic')
interp_by = interp1d(s, by, kind='cubic')
interp_ax = interp1d(s, ax, kind='cubic')
interp_ay = interp1d(s, ay, kind='cubic')


# Calculate interpolated y values
interpolated_bx = interp_bx(s_data)
interpolated_by = interp_by(s_data)
interpolated_ax = interp_ax(s_data)
interpolated_ay = interp_ay(s_data)

for s__,x__, px__, y__, py__,  bx__, by__, ax__, ay__ in zip(s_data, x, px, y, py, interpolated_bx, interpolated_by,  interpolated_ax,  interpolated_ay) :
    x_hat=  bx__**(-.5)*x__
    px_hat= bx__**(-.5)*(bx__*px__ + ax__*x__)
    y_hat=  by__**(-.5)*y__
    py_hat= by__**(-.5)*(by__*py__+ay__*y__)
    print("{:.6} {:.6} {:.6} {:.6} {:.6} {:.6} {:.6} {:.6} {:.6} {:.6} {:.6} {:.6} {:.6}".format(s__, x__, px__, y__, py__, bx__, by__, ax__, ay__, x_hat, px_hat, y_hat, py_hat))

