getfort: get_tune_multiply.o tune_comp_orig.o
	g++ get_tune_multiply.o tune_comp_orig.o -o get_tune_multiply -L/usr/lib -lgfortran -O6
	rm *.o

tune_comp_orig.o : tune_comp_orig.f
	gfortran -c tune_comp_orig.f -o tune_comp_orig.o -fallow-argument-mismatch

get_tune_multiply.o : get_tune_multiply.C
	g++ -c get_tune_multiply.C get_tune_multiply.o -O6

clean:
	rm get_tune_multiply *.o

