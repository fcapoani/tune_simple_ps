C=============================================================
C COMPUTES THE TUNE USING THE INTERPOLATED FFT METHOD
C WITH HANNING FILTER.
C X, XP ARE THE COORDINATES OF THE ORBIT AND MAXN IS THE 
C LENGTH OF THE ORBIT.
C
C AUTHOR:     E. TODESCO - INFN AND CERN 
C

      DOUBLE PRECISION FUNCTION TUNEABT2(X,XP,MAXN)              
      PARAMETER(MAXITER=100000)
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      COMPLEX*16 ZSING(MAXITER)
      DIMENSION X(MAXITER),XP(MAXITER)
      DIMENSION Z(MAXITER)
C..................................ESTIMATION OF TUNE WITH FFT 
      PI=DATAN(1D0)*4D0
      DUEPI=2*PI
      MFT=INT(LOG(FLOAT(MAXN))/LOG(2D0)) 
      NPOINT=2**MFT
      STEP=DUEPI/NPOINT/2D+0
C.............................................................
C      WRITE(*,*) MAXN
C      DO K=1,MAXN
C        WRITE(*,*) X(K), XP(K)
C      ENDDO


      SUM=0D0            !..CHECKS FOR COMPLEX OR REAL DATA
      DO MF=1,NPOINT
        Z(MF)=DCMPLX(X(MF),XP(MF))*DSIN(STEP*MF)**2
        ZSING(MF)=Z(MF)
        SUM=SUM+XP(MF)
      ENDDO
C........................REMOVE AVERAGE
C      ZAV=0D0
C      DO MF=1,NPOINT
C         ZAV=ZAV+ZSING(MF)
C      ENDDO
C      ZAV=ZAV/NPOINT
C      DO MF=1,NPOINT
C         ZSING(MF)=ZSING(MF)-ZAV
C      ENDDO
C.......................FFT
      CALL FFT(ZSING,NPOINT,-1)
C.......................SEARCH FOR MAXIMUM OF FOURIER SPECTRUM
      NPMIN=1
      IF (SUM.EQ.0D0) THEN
        NPMAX=NPOINT/2         !..REAL FFT ONLY HALF COEFFICIENTS
      ELSE
        NPMAX=NPOINT
      ENDIF
C.............................................................
      FTMAX=0D0
      NFTMAX=0
      DO NFT=NPMIN,NPMAX
        IF (ABS(ZSING(NFT)).GT.FTMAX) THEN
          FTMAX=ABS(ZSING(NFT))
          NFTMAX=NFT
        END IF
      ENDDO
      CF1=ABS(ZSING(NFTMAX-1))
      CF2=ABS(ZSING(NFTMAX))
      CF3=ABS(ZSING(NFTMAX+1))
      IF (CF3.GT.CF1) THEN      
        P1=CF2
        P2=CF3
        NN=NFTMAX
      ELSEIF (CF3.LE.CF1) THEN                   
        P1=CF1
        P2=CF2
        NN=NFTMAX-1
      ENDIF
C..........................................INTERPOLATION
      CO=DCOS(2*PI/DFLOAT(NPOINT))
      SI=DSIN(2*PI/DFLOAT(NPOINT))
      SCRA1=CO**2*(P1+P2)**2-2*P1*P2*(2*CO**2-CO-1)       
      SCRA2=(P1+P2*CO)*(P1-P2)
      SCRA3=P1**2+P2**2+2*P1*P2*CO
      SCRA4=(-SCRA2+P2*SQRT(SCRA1))/SCRA3
      ASSK=DFLOAT(NN)+NPOINT/2/PI*ASIN(SI*SCRA4)
      TUNEABT2=1D+0-(ASSK-1D+0)/DFLOAT(NPOINT)
C      TUNEABT1=1D+0-(ASSK-1D+0)/DFLOAT(NPOINT)
C      TUNEABT2=(1D+0-TUNEABT1)*DUEPI
C............................................................  
      RETURN 
C............................................................  
      END

C============================================================
C           COMPUTES THE FFT   (DOUBLE PRECISION)
C           AUTHOR: NUMERICAL RECEIPES, PG. 395
C           NN IS THE NUMBER OF DATA: MUST BE A POWER OF 2
C           ISIGN=1: DIRECT FT
C           ISIGN=-1: INVERSE FT
C           DATA IS A COMPLEX ARRAY WITH THE SIGNAL IN INPUT
C                                   WITH THE FT IN OUTPUT
C           GOOD LUCK, BABY
C

      SUBROUTINE FFT(DATA,NN,ISIGN)
      REAL*8 WR,WI,WPR,WPI,WTEMP,THETA,TEMPI,TEMPR
      REAL*8 DATA(*)

      N=2*NN
      J=1
      DO 11 I=1,N,2
        IF(J.GT.I) THEN
          TEMPR=DATA(J)
          TEMPI=DATA(J+1)
          DATA(J)=DATA(I)
          DATA(J+1)=DATA(I+1)
          DATA(I)=TEMPR
          DATA(I+1)=TEMPI
        END IF
        M=N/2
 1      IF((M.GE.2).AND.(J.GT.M)) THEN
          J=J-M
          M=M/2
        GOTO 1
        END IF
        J=J+M
 11     CONTINUE
      MMAX=2
 2    IF(N.GT.MMAX) THEN
        ISTEP=2*MMAX
        THETA=8.D0*DATAN(1.D0)/(ISIGN*MMAX)
        WPR=-2.D0*DSIN(0.5D0*THETA)**2
        WPI=DSIN(THETA)
        WR=1.D0
        WI=0.D0
        DO 13 M=1,MMAX,2
          DO 12 I=M,N,ISTEP
            J=I+MMAX
            TEMPR=WR*DATA(J)-WI*DATA(J+1)
            TEMPI=WR*DATA(J+1)+WI*DATA(J)
            DATA(J)=DATA(I)-TEMPR
            DATA(J+1)=DATA(I+1)-TEMPI
            DATA(I)=DATA(I)+TEMPR
            DATA(I+1)=DATA(I+1)+TEMPI
 12       CONTINUE
          WTEMP=WR
          WR=WR*WPR-WI*WPI+WR
          WI=WI*WPR+WTEMP*WPI+WI
 13     CONTINUE
        MMAX=ISTEP
        GOTO 2
      END IF
C.............................................................      
      END
