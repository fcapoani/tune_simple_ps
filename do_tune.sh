#!/bin/bash

## ./do_tune.sh ORBIT_FILE NUMBER_OF_TURNS NUMBER_OF_CELLS_PER_TURN
## example: ./do_tune.sh ptest34.dat 12 50

python interp.py $1 $2 > normalized_orbit_$1
awk 'NR % 30 == 1' normalized_orbit_$1 > poincare_orbit_$1
gnuplot -c "plot_orb.gp" normalized_orbit_${1}
cut poincare_orbit_$1 -f 1,10,11,12,13 -d' ' | ./get_tune_multiply 512 $3
